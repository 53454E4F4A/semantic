from django.shortcuts import render, HttpResponseRedirect
from semantic.models import Chapter, Source, Tool, Website, Book, Page
# Create your views here.



def rdfify(request, path="", model="", mid=-1):
    print(path)
    print(model)
    print(mid)
    if path == "" and model == "":
        return render(request, 'semantic/website.rdf', content_type="xml")
    elif len(path) > 0:
        if path == "content":
            description = "Eine Einfuehrung in das Semantic Web"
            title = "Semantic Web - Inhalt"
        if path == "resources":
            description = "Eine Liste von Webseiten, Tools, Buecher und Artikel die sich mit dem Semantic Web befassen."
            title = "Semantic Web - Resourcen"
        if path == "sources":
            description = "Eine Liste von Webseiten, Tools, Buecher und Artikel die zum erstellen dieser Webseite verwendet wurden."
            title = "Semantic Web - Quellen"
        context = {
            "description" : description,
            "title" : title,
            "url" : "http://www.ecksdi.de/semantic/" + path,
            };
        return render(request, 'semantic/electronicdocument.rdf', context, content_type="xml")
    elif len(model) > 0 and int(mid) >= 0:
        if model == "chapter":         
            chapter = Chapter.objects.get(id=int(mid));
            children = Chapter.objects.filter(parent=chapter);
            if chapter.parent != None:
                parentchapter = "http://www.ecksdi.de/semantic/chapter/" + str(chapter.parent.id)
                description = "Kapitel " + str(chapter.parent.number) + "." + str(chapter.number) + " des Semantic Web - Inhalt"
            else:
                parentchapter = None
                description = "Kapitel " + str(chapter.number) + " des Semantic Web - Inhalt"
         
            context = {
                "description" : description,
                "title" : chapter.title,
                "parentchapter" : parentchapter,
                "childrenchapters" : children,
                "url" : "http://www.ecksdi.de/semantic/chapter/" + str(chapter.id),
                };
            return render(request, 'semantic/chapter.rdf', context, content_type="xml")
        if model == "page":         
            page = Page.objects.get(id=int(mid));
          
            context = {
                "page" : page
                };
            return render(request, 'semantic/page.rdf', context, content_type="xml")
        if model == "website":         
            website = Website.objects.get(id=int(mid));
          
            context = {
                "website" : website
                };
            return render(request, 'semantic/rwebsite.rdf', context, content_type="xml")
        if model == "tool":         
            tool = Tool.objects.get(id=int(mid));
          
            context = {
                "tool" : tool
                };
            return render(request, 'semantic/tool.rdf', context, content_type="xml")
        if model == "book":         
            book = Book.objects.get(id=int(mid));
          
            context = {
                "book" : book
                };
            return render(request, 'semantic/book.rdf', context, content_type="xml")
    chapters = Chapter.objects.filter(parent=None)
    print(chapters.first().subchapters)
    context = {
            "chapters" : chapters
            };
    return render(request, 'semantic/content.html', context)

def modelids(request, path="", model="", mid=0):
    if model == "chapter":
        models = Chapter.objects.all()
    if model == "page":
            models = Page.objects.all()
    if model == "website":
            models = Website.objects.all()
    if model == "book":
            models = Book.objects.all()
    if model == "tool":
            models = Tool.objects.all()
     
    context = {
            "mname" : model,
            "models" : models
            };
    return render(request, 'semantic/modelids.html', context)

def index(request):
    chapters = Chapter.objects.filter(parent=None)
    print(chapters.first().subchapters)
    context = {
            "chapters" : chapters
            };
    return render(request, 'semantic/content.html', context)

def modelview(request, model, mid):
    if model == "chapter":
        chapter = Chapter.objects.get(id=mid)
       
        context = {
                "chapter" : chapter
                };
        return render(request, 'semantic/chapter.html', context)
    if model == "page":
        page = Page.objects.get(id=mid)      
        context = {
                "page" : page
                };
        return render(request, 'semantic/page.html', context)
    chapters = Chapter.objects.filter(parent=None)
    print(chapters.first().subchapters)
    context = {
            "chapters" : chapters
            };
    return HttpResponseRedirect("/semantic/resources#"+model+str(mid))

def resources(request):
    chapters = Chapter.objects.filter(parent=None).order_by("number")    
    websites = Website.objects.all()
    tools = Tool.objects.all()
    books = Book.objects.all()
    print(tools)
    context = {
            "chapters" : chapters,
            "tools" : tools,
            "websites" : websites,
            "books" : books
            };
    return render(request, 'semantic/resources.html', context)

def sources(request):
    chapters = Chapter.objects.filter(parent=None).order_by("number")    
    sources = []
    sources.append(Book.objects.first())
    sources.append(Website.objects.get(id=3))
    context = {
            "chapters" : chapters,
            "sources" : sources
            };
    return render(request, 'semantic/sources.html', context)

def code(request):
    chapters = Chapter.objects.filter(parent=None).order_by("number")    
    sources = Source.objects.all()
    print(chapters.first().subchapters)
    context = {
            "chapters" : chapters,
            "sources" : sources
            };
    return render(request, 'semantic/code.html', context)
