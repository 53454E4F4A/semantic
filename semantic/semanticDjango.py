'''
Created on 19.07.2016

@author: jones
'''
from django.db import models
from rdflib import Graph, Literal, BNode, RDF
from pprint import pprint

#class SemanticModel(models.Model):

 #   def toRdf(self, graph, node, rdfType):
  #      graph.add(node, self.rdfField, Literal(str(self)));
   #     for field in self._meta.fields:
    #        pprint(field) 

class SemanticTextField(models.TextField):
    
    def __init__(self, *args, **kwargs):
        self.rdfField = kwargs.pop('rdfField', "")
        super(SemanticTextField, self).__init__(*args, **kwargs)
        
