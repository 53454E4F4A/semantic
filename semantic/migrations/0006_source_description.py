# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-07-22 10:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('semantic', '0005_book_tool_website'),
    ]

    operations = [
        migrations.AddField(
            model_name='source',
            name='description',
            field=models.TextField(blank=True, default='', null=True),
        ),
    ]
