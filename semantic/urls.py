from django.conf.urls import url


from semantic import views

urlpatterns = [
               
  url(r'^semantic/sources$', views.sources, name='sources'),  
  url(r'^semantic/$', views.index, name='index'),  
  url(r'^semantic/content$', views.index, name='index'),  
  url(r'^semantic/resources$', views.resources, name='resources'),  
  url(r'^semantic/code$', views.code, name='index'),  
  url(r'^semantic/$', views.code, name='index'),  
  url(r'^semantic/(?P<path>\w+[sources|content|resources])/rdf$', views.rdfify, name='index'),    
  url(r'^semantic/rdf$', views.rdfify, name='index'),  
  url(r'^semantic/(?P<model>\w+[chapter|page|tool|website|book])/(?P<mid>\d)/rdf$', views.rdfify, name='index'),           
  url(r'^semantic/(?P<model>\w+[chapter|page|tool|website|book])/ids$', views.modelids, name='index'),   
  url(r'^semantic/(?P<model>\w+[chapter|page|tool|website|book])/(?P<mid>\d)$', views.modelview, name='index'),                     



     

]   