from django.contrib import admin

from .models import Chapter, Page, Source, Website, Tool, Book


admin.site.register(Chapter)
admin.site.register(Page)
admin.site.register(Source)
admin.site.register(Website)
admin.site.register(Tool)
admin.site.register(Book)