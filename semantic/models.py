from django.db import models
from semantic.semanticDjango import SemanticTextField#, SemanticModel
from django.utils.encoding import python_2_unicode_compatible
from rdflib import Graph, Literal, BNode, RDF
from rdflib.namespace import FOAF, DC
# Create your models here.
@python_2_unicode_compatible  
class Chapter(models.Model): 
    parent = models.ForeignKey("Chapter", null=True, blank=True, related_name='subchapters'); 
    title = models.TextField(default="", null=True, blank=True);    
    number = models.IntegerField(default=0);
    def __str__(self):
        return u'{0}'.format(str(self.title))
    
class Page(models.Model):
    chapter = models.ForeignKey("Chapter", null=True, blank=True, related_name='pages');     
    number = models.IntegerField(default=0);
    content = models.TextField(default="", null=True, blank=True);
   
  
    def __str__(self):       
        return u'{0}'.format(str(self.content))
    
class Source(models.Model):
    type = models.TextField(default="", null=True, blank=True);
    name = models.TextField(default="", null=True, blank=True);
    content = models.TextField(default="", null=True, blank=True);
    description = models.TextField(default="", null=True, blank=True);
    def __str__(self):       
        return u'{0}'.format(str(self.content))
    
class Website(models.Model):    
    name = models.TextField(default="", null=True, blank=True);
    link = models.TextField(default="", null=True, blank=True);
    description = models.TextField(default="", null=True, blank=True);
    def __str__(self):       
        return u'{0}'.format(str(self.name))    
class Tool(models.Model):
    name = models.TextField(default="", null=True, blank=True);
    link = models.TextField(default="", null=True, blank=True);
    description = models.TextField(default="", null=True, blank=True);
    def __str__(self):       
        return u'{0}'.format(str(self.name))
class Book(models.Model):
    name = models.TextField(default="", null=True, blank=True);
    link = models.TextField(default="", null=True, blank=True);
    description = models.TextField(default="", null=True, blank=True);
    def __str__(self):       
        return u'{0}'.format(str(self.name))